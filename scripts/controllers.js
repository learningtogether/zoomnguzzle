var guzzleModule = angular.module("Guzzle", []);

guzzleModule.factory('FillUps', function(){
    var fillUps = {};
    fillUps.getAllFillUps = function(){
        return [
            { id:0, date:"2013-12-17", costInUSD: 32.04, amountInUSGal: 10.683, odo: 37654, comments:"Highway"},
            { id:1, date:"2013-12-21", costInUSD: 39.33, amountInUSGal: 11.240, odo: 38015, comments:"Highway"},
            { id:2, date:"2013-12-21", costInUSD: 34.95, amountInUSGal: 10.594, odo: 38333, comments:"Highway"},
            { id:3, date:"2013-12-27", costInUSD: 34.73, amountInUSGal: 11.207, odo: 38684, comments:"Highway"}
        ];
    };
    return fillUps;
});

guzzleModule.controller('RecordsController', function($scope, FillUps){
    //Get a "local" copy of the fillups
    var fillUps = FillUps.getAllFillUps();

    $scope.calculateDistanceBetweenFillUps = function(newReading, oldReading){
        return newReading - oldReading;
    };

    $scope.calculateEfficiencyForFillUp = function(distance, amount) {
        return distance / amount;
    }

    //Create a list of the fillups augmented with calculated efficiency and distance
    //travelled fields
    $scope.initializeList = function(){
        var len, i;
        for(i = 0, len = fillUps.length; i < len; i++){
            
            fillUps[i].distance = 0;
            fillUps[i].efficiency = 0;
            if(i > 0){
                fillUps[i].distance = $scope.calculateDistanceBetweenFillUps(fillUps[i].odo, fillUps[i-1].odo);
                fillUps[i].efficiency = $scope.calculateEfficiencyForFillUp(fillUps[i].distance, fillUps[i].amountInUSGal);
            }
        }
    }();    

    $scope.list = fillUps;

    $scope.getNextId = function() {
        return fillUps.length > 0 ? fillUps[fillUps.length-1].id+1 : 0;
    };

    $scope.getMinimumOdometerReading = function(){
        return fillUps.length > 0 ? fillUps[fillUps.length-1].odo : 0;
    };

    $scope.getLastFillupDate = function() {
        return fillUps.length > 0 ? fillUps[fillUps.length-1].date : "";
    };

    $scope.getTodaysDateFormatted = function() {
        return (new Date()).toISOString().substring(0,10);
    };

    $scope.minimumVolume = 0.1;
    $scope.minimumPrice = 0; //otherwise known as free gas, or a lie

    $scope.getEmptyRecord = function() {
        return {
            id: $scope.getNextId(),
            date: $scope.getTodaysDateFormatted(),
            costInUSD: $scope.minimumPrice,
            amountInUSGal: $scope.minimumVolume,
            odo: $scope.getMinimumOdometerReading(),
            comments: ""
        };  
    };

    $scope.newRecord = $scope.getEmptyRecord();
    $scope.newRecordFormVisible = false;
    

    $scope.resetNewRecord = function(){
        $scope.newRecord = $scope.getEmptyRecord();
        $scope.newRecordFormVisible = false;
    };

    $scope.toggleAddNewFormVisibility = function() {
        $scope.newRecordFormVisible = !$scope.newRecordFormVisible;                
    };

    $scope.addNewRecord = function() {
        if(fillUps.length > 0){            
            $scope.newRecord.distance = $scope.calculateDistanceBetweenFillUps($scope.newRecord.odo, fillUps[fillUps.length -1].odo);
            $scope.newRecord.efficiency = $scope.calculateEfficiencyForFillUp($scope.newRecord.distance, $scope.newRecord.amountInUSGal);
        }
        else{
            $scope.newRecord.distance = 0;
            $scope.newRecord.efficiency = 0;
        }

        fillUps.push($scope.newRecord);
        $scope.resetNewRecord();
    };

});